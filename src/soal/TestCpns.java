package soal;

import data.DataJawaban;
import data.DataPendaftar;

import data.DataSoal;
import java.util.ArrayList;
import java.util.Scanner;
import static menu.Menu.clearScreen;
import static menu.Menu.tampilan;

public class TestCpns {
    private static boolean bukaSeleksi = false;
    

    public static void setBukaSeleksi(boolean bukaSeleksi) {
        TestCpns.bukaSeleksi = bukaSeleksi;
    }

    public static boolean isBukaSeleksi() {
        return bukaSeleksi;
    }

    
    public static void mulaiTes(int indeks){
        Scanner input = new Scanner(System.in);
        if ( DataSoal.getArraySoal().isEmpty() ){
            tampilan('+','=', 50);
            System.out.println("|       Soal Tes CPNS Belum tersedia saat ini.     |");
            tampilan('+','=', 50);
            
            
        } else {
            System.out.println("Nilai per soal: " + DataSoal.getArrayIndex(0).getNilai());
            tampilan('+','=', 50);
            System.out.println("|                Memulai Tes CPNS 2020             |");
            System.out.println("|   Anda Tidak Dapat Kembali Hingga Tes Selesai.   |");
            tampilan('+','=', 50);
            System.out.println("     Jumlah Soal: " + DataSoal.getArraySoal().size());
            System.out.print  (   "\n  Mulai tes? ( y / t ): ");
            String mulai = input.nextLine().toLowerCase();

            if ( mulai.equals("y") ){
                jawabSoal(indeks);
            } else if ( !mulai.equals("t") ){
                tampilan('+','=', 50);
                System.out.println("|       Input yang anda masukkan tidak benar       |");
                tampilan('+','=', 50);
            }
            
        }
    }
    
    
    public static void jawabSoal(int indeks){
        ArrayList<String> jawaban_sementara = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        
        System.out.println("Soal Tes CPNS");
        for (int i = 0; i < DataSoal.getArraySoal().size()   ; i++){
            System.out.println(   (i+1) + "\t" + DataSoal.getArrayIndex(i).getQuestion());
            System.out.println(     " " + "\t" + "Nilai per soal: " + DataSoal.getArrayIndex(i).getNilai());
            System.out.println(     " " + "\ta. " + DataSoal.getArrayIndex(i).getJawaban_1());
            System.out.println(     " " + "\tb. " + DataSoal.getArrayIndex(i).getJawaban_2());
            System.out.println(     " " + "\tc. " + DataSoal.getArrayIndex(i).getJawaban_3());
            System.out.println(     " " + "\td. " + DataSoal.getArrayIndex(i).getJawaban_4());
            System.out.println(     " " + "\te. " + DataSoal.getArrayIndex(i).getJawaban_5());
            System.out.print  (   "\n " + "\tJawaban Anda ( a / b / c / d / e ): ");
            
            String input_jawaban = input.nextLine().toLowerCase();
            jawaban_sementara.add(input_jawaban);
        }
          
       clearScreen();
       boolean loop = true;
       while(loop){
            System.out.println("Soal Tes CPNS");
            for (int i = 0; i < DataSoal.getArraySoal().size()   ; i++){
                System.out.println(   (i+1) + "\t" + DataSoal.getArrayIndex(i).getQuestion());
                System.out.println(     " " + "\ta. " + DataSoal.getArrayIndex(i).getJawaban_1());
                System.out.println(     " " + "\tb. " + DataSoal.getArrayIndex(i).getJawaban_2());
                System.out.println(     " " + "\tc. " + DataSoal.getArrayIndex(i).getJawaban_3());
                System.out.println(     " " + "\td. " + DataSoal.getArrayIndex(i).getJawaban_4());
                System.out.println(     " " + "\te. " + DataSoal.getArrayIndex(i).getJawaban_5());
                System.out.println  (   "\n " + "\tJawaban Anda : " + jawaban_sementara.get(i));

            }
            tampilan('+','=', 50);
            System.out.println("| 1. Selesai Tes                                   |");
            tampilan('+','-', 50);
            System.out.println("| 2. Ubah Jawaban                                  |");
            tampilan('+','=', 50);

            System.out.print(" Masukan Pilihan : ");
            String opsi = input.nextLine();

            switch (opsi) {
                case "1":
                    
                    tampilan('+','=', 50);
                    System.out.println("|               Konfirmasi selesai tes             |");
                    System.out.println("|       JAWABAN TIDAK AKAN DAPAT DIUBAH LAGI       |");
                    tampilan('+','=', 50);
                    System.out.print("\n  Anda yakin ingin menyelesaikan tes ini? ( y / t ) : ");
                    String selesai = input.nextLine().toLowerCase();

                    tampilan('+','=', 50);

                    if ( selesai.equals("y") ){
                        
                        DataJawaban.updateData(indeks, jawaban_sementara);
                         
                        
                        tampilan('+','=', 50);
                        System.out.println("|                TES TELAH SELESAI.                |");
                        System.out.println("|        Semoga anda lolos tahap berikutnya        |");
                        tampilan('+','=', 50);
                        loop = false;
                        
                    } else if ( !selesai.equals("t") ){
                        clearScreen();
                        System.err.println("Pilihan Tidak ada");
                    }

                    break;

                case "2":
                    loop = false;
                    clearScreen();
                    jawabSoal(indeks);
                    break;

                default:
                    System.err.println("Pilihan Tidak ada");
                    break;
            }
        }
    }
    
    public static void lihatHasilSeleksi(int indeks) {
        if ( bukaSeleksi == true){
            float nilai = DataJawaban.getArrayIndex(indeks).getNilai();
            tampilan('+','=', 59);
            System.out.println("|                HASIL SELEKSI TES CPNS 2020                |");
            tampilan('+','-', 59);
            System.out.println("Nama          : " + DataPendaftar.getArrayIndex(indeks).getNama() );
            System.out.println("NIK           : " + DataPendaftar.getArrayIndex(indeks).getNik());
            System.out.println("No. KK        : " + DataPendaftar.getArrayIndex(indeks).getNo_kk() );
            System.out.println("Tanggal lahir : " + 
                DataPendaftar.getArrayIndex(indeks).getTanggal()  + " - "  + 
                DataPendaftar.getArrayIndex(indeks).getBulan()    + " - "  + 
                DataPendaftar.getArrayIndex(indeks).getTahun() );
            tampilan('+','-', 59);
            System.out.println("            NILAI ANDA: " + nilai);
            tampilan('+','-', 59);
            
            if ( nilai >= 80.0 ){
                
                System.out.println("|          SELAMAT ANDA LOLOS TAHAP TES CPNS 2020!          |");
                System.out.println("| Silahkan menunggu info berikutnya untuk tahap selanjutnya |");
            } else {
                System.out.println("|           MOHON MAAF ANDA TIDAK LULUS TAHAP TES           |");
                System.out.println("|        Jangan berkecil hati. Masih ada tahun depan        |");
                
            }
            
            
            
            tampilan('+','=', 59);
            
        } else {
            tampilan('+','=', 52);
            System.out.println("|       Hasil Seleksi Tes CPNS belum keluar.       |");
            System.out.println("|      Hasil keluar pada tanggal 1 Juli 2020.      |");
            tampilan('+','=', 52);
        }
    }
}
