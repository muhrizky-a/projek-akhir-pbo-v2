package data;

import java.util.ArrayList;
import java.util.Scanner;
import static menu.Menu.tampilan;

import soal.Soal;


public class DataSoal extends DataUpdate{
    
    private static ArrayList<Soal> arraySoal = new ArrayList<>();
    Scanner input = new Scanner(System.in);
    Scanner inputInt = new Scanner(System.in);
    
    @Override
    public void tambahData() {
        String pertanyaan, jawaban_1, jawaban_2, jawaban_3, 
                jawaban_4, jawaban_5, jawabanBenar;
        
        
        System.out.print("\nPertanyaan: ");
        pertanyaan = input.nextLine();
        
        System.out.print("\nOpsi Jawaban ( a. ): ");
        jawaban_1 = input.nextLine();
        
        System.out.print("\nOpsi Jawaban ( b. ): ");
        jawaban_2 = input.nextLine();
        
        System.out.print("\nOpsi Jawaban ( c. ): ");
        jawaban_3 = input.nextLine();
        
        System.out.print("\nOpsi Jawaban ( d. ): ");
        jawaban_4 = input.nextLine();

        System.out.print("\nOpsi Jawaban ( e. ): ");
        jawaban_5 = input.nextLine();
        
        System.out.print("\nJawaban Benar ( a / b / c / d / e ): ");
        jawabanBenar = input.nextLine().toLowerCase();
        
        tambahData(pertanyaan, jawaban_1, jawaban_2, jawaban_3, jawaban_4, jawaban_5, jawabanBenar);
        
        System.out.print("Penambahan data sukses\n\n\n");
    }
    
    public static void tambahData(
            String question, String jawaban_1, String jawaban_2, 
            String jawaban_3, String jawaban_4, String jawaban_5, 
            String jawabanBenar) {    
        
        arraySoal.add( new Soal(
                question, jawaban_1, jawaban_2, 
                jawaban_3, jawaban_4, jawaban_5, 
                jawabanBenar
                )
        );
        
        float nilai = (float) (100.0 / getArraySoal().size());
        for (int i = 0; i < getArraySoal().size()   ; i++){
            getArrayIndex(i).setNilai(nilai);                    
        }
    }

    
    public static ArrayList<Soal> getArraySoal() {
        return arraySoal;
    }
    
    public static Soal getArrayIndex(int i) {
        return arraySoal.get(i);
    }
    
    @Override
    public void lihatData() {
        System.out.println("Soal Tes CPNS");
        if ( !getArraySoal().isEmpty() ){
            
            for (int i = 0; i < getArraySoal().size()   ; i++){
                System.out.println(   (i+1) + "\t" + getArrayIndex(i).getQuestion());
                System.out.println(     " " + "\t" + "( Jawaban benar: " + getArrayIndex(i).getJawabanBenar() 
                        +" , Nilai per soal: " + getArrayIndex(i).getNilai() + " )");
                System.out.println(     " " + "\ta. " + getArrayIndex(i).getJawaban_1());
                System.out.println(     " " + "\tb. " + getArrayIndex(i).getJawaban_2());
                System.out.println(     " " + "\tc. " + getArrayIndex(i).getJawaban_3());
                System.out.println(     " " + "\td. " + getArrayIndex(i).getJawaban_4());
                System.out.println(     " " + "\te. " + getArrayIndex(i).getJawaban_5() + "\n\n");
                                
            }
            
        } else {
            tampilan('+','=', 50);
            System.out.println("|                 Data Soal Kosong                 |");
            tampilan('+','=', 50);
            
        }
        
    }
    
    
    
    public int cariData() {
        int indeks = -1;
        if ( !getArraySoal().isEmpty() ){
            try {
                lihatData();
                System.out.print("Pilih nomor soal: ");
                String inputSoal = input.nextLine();
                indeks = Integer.parseInt(inputSoal);

            } catch (NumberFormatException e){

                tampilan('+','=', 50);
                System.out.println(" Pilihan Tidak ada");
                tampilan('+','=', 50);

            }
        
            if ( indeks != -1 ){
                return indeks;
            } else {
                return -1;
            }
        } else {
            tampilan('+','=', 50);
            System.out.println("|                 Data Soal Kosong                 |");
            tampilan('+','=', 50);
            return -2;
        }
        
    }
    
    @Override
    public void updateData() {
        int indeks = cariData();
        if( indeks > 0 && indeks <= getArraySoal().size() ){                
            System.out.println("Ubah Soal Tes no. " + indeks );
            updateData(indeks-1);
            
        } else if ( indeks != -2 ) {
            System.out.println("Maaf soal yang anda cari tidak ditemukan\n\n");
        }
    }
    
    public void updateData(int indeks) {
        String pertanyaan, jawaban_1, jawaban_2, jawaban_3, 
                jawaban_4, jawaban_5, jawabanBenar;
        
        
        System.out.print("\nPertanyaan: ");
        pertanyaan = input.nextLine();
        
        System.out.print("\nOpsi Jawaban ( a. ): ");
        jawaban_1 = input.nextLine();
        
        System.out.print("\nOpsi Jawaban ( b. ): ");
        jawaban_2 = input.nextLine();
        
        System.out.print("\nOpsi Jawaban ( c. ): ");
        jawaban_3 = input.nextLine();
        
        System.out.print("\nOpsi Jawaban ( d. ): ");
        jawaban_4 = input.nextLine();

        System.out.print("\nOpsi Jawaban ( e. ): ");
        jawaban_5 = input.nextLine();
        
        System.out.print("\nJawaban Benar ( a / b / c / d / e ): ");
        jawabanBenar = input.nextLine();
        
        getArrayIndex(indeks).setQuestion(pertanyaan);
        getArrayIndex(indeks).setJawaban_1(jawaban_1);
        getArrayIndex(indeks).setJawaban_2(jawaban_2);
        getArrayIndex(indeks).setJawaban_3(jawaban_3);
        getArrayIndex(indeks).setJawaban_4(jawaban_4);
        getArrayIndex(indeks).setJawaban_5(jawaban_5);
        getArrayIndex(indeks).setJawabanBenar(jawabanBenar);
        
        System.out.print("Perubahan data sukses\n\n\n");
    }

    @Override
    public void hapusData() {
        int indeks = cariData();
        if( indeks > 0 && indeks <= getArraySoal().size() ){                
            System.out.println("Soal tes no. " + indeks +  " berhasil dihapus");
            getArraySoal().remove(indeks-1);
            
            if (!getArraySoal().isEmpty()){
                float nilai = (float) (100.0 / getArraySoal().size());
                for (int i = 0; i < getArraySoal().size(); i++){
                    getArrayIndex(i).setNilai(nilai);                    
                }
            }
        } else if ( indeks != -2 ) {
            System.out.println("Maaf soal yang anda cari tidak ditemukan\n\n");
        }
    }

    
    
}
