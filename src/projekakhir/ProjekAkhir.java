package projekakhir;
import menu.*;

public class ProjekAkhir {
    
    private static int kesempatan_login = 3;
    
    public static int getKesempatan_login() {
        return kesempatan_login;
    }

    public static void setKesempatan_login(int kesempatan_login) {
        ProjekAkhir.kesempatan_login = kesempatan_login;
    }
    
    public static void main(String[] args) {
        
        
        Menu menuAwal = new Menu();
        menuAwal.tampilkanMenu();
    }
    
}
