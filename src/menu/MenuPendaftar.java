package menu;

import data.DataJawaban;
import data.DataPendaftar;
import soal.TestCpns;

import java.util.Scanner;



public class MenuPendaftar extends Menu{
    
    Menu menuAwal;
    DataPendaftar dataAkun = new DataPendaftar();
    DataJawaban dataJawaban = new DataJawaban();
    
    int indeks = 0, kesempatan_login = 3;
    Scanner input = new Scanner(System.in);
    
    public MenuPendaftar() {
        this.tampilkanMenu();
    }
    
    
    void daftarAkun(){
        String nama, nik, no_kk, 
            tanggal, bulan, tahun, 
            email, password;
        
        System.out.print("\nNama Lengkap: ");
        nama = input.nextLine();
        
        System.out.print("\nNIK: ");
        nik = input.nextLine();
        
        System.out.print("\nNo. KK: ");
        no_kk = input.nextLine();
        
        System.out.print("\nTanggal lahir: ");
        tanggal = input.nextLine();
        
        System.out.print("\nBulan lahir: ");
        bulan = input.nextLine();
        
        System.out.print("\nTahun lahir: ");
        tahun = input.nextLine();

        System.out.print("\nBuat Password Akun: ");
        password = input.nextLine();
        
        DataPendaftar.tambahData(nama, nik, no_kk, tanggal, bulan, tahun, password, 1);
        
        System.out.print("Penambahan data sukses\n\n\n");
        tampilkanMenu();
    }
    
    void login(){
        
        boolean loginLoop = true, loginStatus = false;
        while ( loginLoop ){
            System.out.println("Kesempatan login: " + kesempatan_login );
            System.out.print("NIK      : ");
            String id = input.nextLine();
            
            System.out.print("Password : ");
            String pass = input.nextLine();
            
            for (int i=0; i< DataPendaftar.getArrayPendaftar().size();i++){
                if ( DataPendaftar.getArrayIndex(i).getNik().equals( id ) && 
                        DataPendaftar.getArrayIndex(i).getPassword().equals( pass )) {
                    indeks=i;
                    loginStatus = true;
                }
            }
            if( loginStatus == true ){
                loginLoop = false;
                tampilkanMenu( indeks );
                    
            }else{
                kesempatan_login -= 1;
                System.out.println("\nNIK / Password anda salah");
            }

            if( kesempatan_login == 0 ){
                loginLoop = false;
                menuAwal = new Menu();
                menuAwal.tampilkanMenu();
            }
	}
    }
    void lupaPassword(){
        boolean resetPassword = false;
        String nik, tanggal, bulan, tahun, password;
        
        tampilan('+','=', 50);
        System.out.println("|                  LUPA PASSWORD?                  |");
        tampilan('+','=', 50);
        
        System.out.print("\nNIK: ");
        nik = input.nextLine();
        
        System.out.print("\nTanggal lahir: ");
        tanggal = input.nextLine();
        
        System.out.print("\nBulan lahir: ");
        bulan = input.nextLine();
        
        System.out.print("\nTahun lahir: ");
        tahun = input.nextLine();

        

        for (int i=0; i< DataPendaftar.getArrayPendaftar().size();i++){
            if ( DataPendaftar.getArrayIndex(i).getNik().equals( nik ) && 
                DataPendaftar.getArrayIndex(i).getTanggal().equals( tanggal ) &&
                    DataPendaftar.getArrayIndex(i).getBulan().equals( bulan ) &&
                    DataPendaftar.getArrayIndex(i).getTahun().equals( tahun ) ) {
                indeks=i;
                resetPassword = true;
            }
        }
        if( resetPassword == true ){
            tampilan('+','=', 50);
            System.out.println("|                  RESET PASSWORD                  |");
            tampilan('+','=', 50);
            System.out.println("Data ditemukan");
            System.out.println("Nama: " + DataPendaftar.getArrayIndex(indeks).getNama());
            System.out.println("NIK : " + DataPendaftar.getArrayIndex(indeks).getNik());
            System.out.print("\nBuat Password Baru: ");
            password = input.nextLine();
            DataPendaftar.getArrayIndex(indeks).setPassword(password);
            
            System.out.print("Perubahan password sukses\n\n\n");

        }else{
            System.out.println("\nNIK / tanggal lahir tidak cocok");
        }
        tampilkanMenu();
    }
    
    @Override
    public void tampilkanMenu(){
        boolean loop = true;
        while(loop){
            tampilan('+','=', 50);
            System.out.println("| 1. Daftar Akun                                   |");
            System.out.println("| 2. Login Akun                                    |");
            System.out.println("| 3. Lupa Password?                                |");
            System.out.println("| 4. Kembali ke menu awal                          |");
            tampilan('+','=', 50);
            
            System.out.print("Masukan Pilihan : ");
            String opsi;
            opsi = input.nextLine();
    
            switch (opsi) {
                case "1":
                    loop = false;
                    clearScreen();
                    daftarAkun();
                    break;
                    
                case "2":
                    loop = false;
                    clearScreen();
                    login();
                    break;
                    
                case "3":
                    loop = false;
                    clearScreen();
                    lupaPassword();
                    
                    break;
                    
                case "4":
                    loop = false;
                    clearScreen();
                    menuAwal = new Menu();
                    menuAwal.tampilkanMenu();
                    break;
                    
                default:
                    System.err.println("Pilihan Tidak ada");
                    break;
            }
            
        }
    }
    public void tampilkanMenu( int indeks ){
        System.out.println(" Selamat Datang, " + DataPendaftar.getArrayIndex(indeks).getNama());
        boolean loop = true;
        while(loop){
            tampilan('+','=', 50);
            if(DataJawaban.getArrayIndex(indeks).isTelahIkutTes() == true){
                System.out.println("| 1. Hasil Seleksi                                 |");
            } else {
                System.out.println("| 1. Mulai Tes                                     |");
            }
            System.out.println("| 2. Lihat Data Akun                               |");
            System.out.println("| 3. Ubah Data Akun                                |");
            System.out.println("| 4. Kembali ke menu awal                          |");
            tampilan('+','=', 50);
            
            System.out.print("Masukan Pilihan : ");
            String opsi = input.nextLine();
    
            switch (opsi) {
                case "1":
                    loop = false;
                    clearScreen();
                    if(DataJawaban.getArrayIndex(indeks).isTelahIkutTes() == true){
                        TestCpns.lihatHasilSeleksi(indeks);
                    } else {
                        TestCpns.mulaiTes(indeks);
                    }
                    
                    
                    tampilkanMenu(indeks);
                    break;
                    
                case "2":
                    loop = false;
                    clearScreen();
                    dataAkun.lihatData(indeks);
                    tampilkanMenu(indeks);
                    break;
                    
                case "3":
                    loop = false;
                    clearScreen();
                    int kesempatan_ubah = DataPendaftar.getArrayIndex(indeks).getKesempatan_ubah_data();
                    if ( kesempatan_ubah == 0 || DataJawaban.getArrayIndex(indeks).isTelahIkutTes() == true){
                        tampilan('+','=', 50);
                        System.out.println("|         Kesempatan ubah data telah habis         |");
                        System.out.println("|                       ATAU                       |");
                        System.out.println("|             Anda telah mengikuti tes             |");
                        tampilan('+','-', 50);
                        System.out.println("|       Data Tidak Dapat Diubah - Ubah Lagi.       |");
                        tampilan('+','=', 50);
                    } else {
                        tampilan('+','=', 50);
                        System.out.println("| Kesempatan ubah data: " + kesempatan_ubah + "                          |");
                        tampilan('+','=', 50);

                        dataAkun.updateData(indeks);
                    }
                    
                    tampilkanMenu(indeks);
                    break;
                    
                case "4":
                    loop = false;
                    clearScreen();
                    menuAwal = new Menu();
                    menuAwal.tampilkanMenu();
                    break;
                    
                default:
                    System.err.println("Pilihan Tidak ada");
                    break;
            }
            
        }
    }
}
